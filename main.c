#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "btree.h"

typedef int my_item;


char my_compare_item_keys(void* p_item1, void* p_item2)
{
    my_item item1 = *((my_item*) p_item1);
    my_item item2 = *((my_item*) p_item1);

    if (item1 < item2)
        return -1;
    else if (item2 < item1)
        return 1;
    else
        return 0;
}


void init()
{
    size_t my_item_size = sizeof(my_item);
    unsigned char order = 2;
    compare_items_t p_compare_items = &my_compare_items;

    bt_t* p_my_btree = create_btree(my_item_size, order, p_compare_items);

    my_item my_new_item;

    add_item(p_my_btree, (void*)(&my_new_item));
    remove_item(p_my_btree, (void*)(&my_new_item));

    destroy_tree(p_my_btree);

}


unsigned int value(void * key)
{
    return *((int *)key);
}

unsigned int keysize(void * key)
{
    return sizeof(int);
}

unsigned int datasize(void * data)
{
    return sizeof(int);
}

int main(int argc,char * argv[])
{
    int i = 0;
    btree * tree;
    bt_key_val * kv;
    int item = 0x43;
    int count;
    int order;
    int * values;

    srand(time(NULL));

    for(order=3; order<60; order++)
    {
        count = rand()%60;
        values = (int *)malloc(count*sizeof(int));

        for(i = 0; i<count; i++)
        {
            values[i] = rand()%1024;
        }

        tree = btree_create(order);
        tree->value = value;
        tree->key_size = keysize;
        tree->data_size = datasize;

        for (i=0; i<count; i++)
        {
            kv = (bt_key_val*)malloc(sizeof(*kv));
            kv->key = malloc(sizeof(int));
            *(int *)kv->key = values[i];
            kv->val = malloc(sizeof(int));
            *(int *)kv->val = values[i];
            btree_insert_key(tree,kv);
        }

        print_statistic_subtree(tree);
        pretty_print_subtree(tree, tree->root, NULL);
        //print_subtree(tree,tree->root);

        for (i= count - 1; i > -1; i-= (rand()%5))
        {
            item = values[i];
            btree_delete_key(tree,tree->root,&item);
        }

        free(values);
        btree_destroy(tree);
    }
    return 0;
}
