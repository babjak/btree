#include "btree.h"

#include <stdlib.h>
#include <string.h>

/*
typedef struct
{
    bt_node_t*      node;
    item_counter_type        idx;
} node_pos_t;
*/

/*******************************************************************************
*
*                               HELPER FUNCTIONS
*
*******************************************************************************/
/*******************************************************************************
*	Takes a buffer of items and returns a pointer to the item specified with
*   the index.
*
*	@param  buffer      Buffer containing items
*	@param  item_size   Size of a single item
*	@param  idx         Index of the item of interest
*
*	@return             Pointer to item of interest
*******************************************************************************/
void* get_item_pointer( void*				buffer,
                        size_t				item_size,
                        item_counter_type	idx )
{
    size_t  item_size_in_char   = item_size / sizeof(char);
    size_t  offset_in_char      = idx * item_size_in_char;

    return (void*) ( ((char*) buffer ) + offset_in_char );
}


/*******************************************************************************
*	Determines whether a node is leaf node by looking at it's first child.
*   A leaf node won't have any children, thus first child will be NULL.
*
*	@param  node        Pointer to the node of interest
*
*	@return             1 if node is a leaf, 0 otherwise
*******************************************************************************/
char is_leaf( bt_node_t* p_n )
{
    return (p_n->kids ? 0 : 1);
}


/*******************************************************************************
*	Determines whether a node has reached the maximum amount of items it can
*   store.
*
*	@param  tree        Pointer to the tree the node is part of
*	@param  p_n        Pointer to the node of interest
*
*	@return             1 if node is full, 0 otherwise
*******************************************************************************/
char is_full( bt_t*         p_t,
              bt_node_t*    p_n )
{
    return ((p_n->nr_active == 2*p_t->order - 1) ? 1 : 0);
}


/*******************************************************************************
*
*                                  CREATION
*
*******************************************************************************/
/*******************************************************************************
*	Creates a B-Tree node, allocates memory.
*
*	@param  order       Limits kids and item numbers in the node:
*                           2*order = max active kids
*                           2*order - 1 = max number of items
*	@param  item_size   Size of a single item
*
*	@return             Pointer to a new, empty, leaf node
*******************************************************************************/
bt_node_t* create_btree_node(order_t		order, 
							 size_t			item_size, 
							 char			leaf)
{
    bt_node_t*  p_n    = (bt_node_t*) malloc(sizeof(bt_node_t));

//    p_n->level		= 0;
    p_n->nr_active	= 0;
//    p_n->next		= NULL;

    p_n->items = malloc( (2*order - 1) * item_size );

    if (leaf)
        p_n->kids = NULL; // Used to determine whether the node is a leaf
    else
        p_n->kids = (bt_node_t**) malloc( 2*order * sizeof(bt_node_t*) );

//    size_t kids_buffer_size = 2*order * sizeof(bt_node_t*);
//    node->kids = (bt_node_t**) malloc( kids_buffer_size );
//    memset((void*) p_node->kids, 0, kids_buffer_size);

    return p_n;
}


/*******************************************************************************
*	Creates a B-Tree, allocates memory.
*
*	@param  order       Limits children and item numbers in one node:
*                           2*order = max active kids
*                           2*order - 1 = max number of items
*	@param  item_size   Size of a single item
*	@param  comp_items  Function pointer to a function that compares items
*
*	@return             Pointer to a new, empty B-Tree
*******************************************************************************/
bt_t* create_btree(
    order_t             order,
    size_t              item_size,

//function pointers
    compare_items_t     comp_items
)
{
    bt_t*   p_t			= (bt_t*) malloc(sizeof(bt_t));

    p_t->order			= order;
    p_t->item_size		= item_size;
    p_t->comp_items		= comp_items;

    p_t->root			= create_btree_node(order, item_size, 1); //create first leaf node

    return p_t;
}


/*******************************************************************************
*
*                                  DESTRUCTION
*
*******************************************************************************/
/*******************************************************************************
*	Destroys a particular B-Tree node, frees up memory. 
*	WARNING! NOT RECURSIVE!
*   Child nodes should be destroyed first, to avoid memory leaks.
*
*	@param  p_n        Pointer to the node that will get deleted
*******************************************************************************/
void free_btree_node ( bt_node_t* p_n )
{
    if ( !is_leaf(p_n) )
        free( p_n->kids );

    free( p_n->items );
    free( p_n );
}


/*******************************************************************************
*
*                                   INSERTION
*
*******************************************************************************/
/*******************************************************************************
*	Splits the child node specified by an index in half.
*
*	@param  p_t        Pointer to the tree the node is part of
*	@param  p_n        Pointer to the parent node of interest
*	@param  idx         Index to the child that should be split up
*******************************************************************************/
void split_full_child( bt_t*				p_t,
						bt_node_t*			p_n,
						item_counter_type	idx )
{
	void*       src;
    void*       dest;

    order_t     order;
    size_t      item_size;
    bt_node_t*  child;
    bt_node_t*  new_child;


//if current node is full we cannot split any child
	if ( is_full(p_t, p_n) )
		return;

//if child is not full, we're not splitting either
	if ( !is_full(p_t, p_n->kids[idx]) )
		return;

//OK, let's do this
	order		= p_t->order;
    item_size   = p_t->item_size;
    child       = p_n->kids[idx];
    new_child   = create_btree_node( order, item_size, is_leaf(child) );

//1. Take care of kids, copy half of the items to new child
//    new_child->level = child->level;

    new_child->nr_active = child->nr_active / 2;
    child->nr_active = (child->nr_active - 1) / 2;


    // Copy the higher order items to the new child
    src = get_item_pointer( child->items, item_size, child->nr_active + 1 );
    dest = new_child->items;
    memcpy( dest, src, new_child->nr_active * item_size );

    // Copy the higher order pointers to the new child
    if ( !is_leaf(child) )
    {
        src = (void*) &child->kids[child->nr_active + 1];
        dest = (void*) new_child->kids;
        memcpy( dest, src, (new_child->nr_active + 1) * sizeof(bt_node_t*) );
    }


//2. Take care of parent
    //Move items to make place for new item and copy new item
    if (p_n->nr_active)
    {
        src = get_item_pointer( p_n->items, item_size, idx + 1 );
        dest = get_item_pointer( p_n->items, item_size, idx + 2 );
        memmove( dest, src, (p_n->nr_active - idx - 1) * item_size );
    }

    src = get_item_pointer( child->items, item_size, child->nr_active );
    dest = get_item_pointer( p_n->items, item_size, idx );
    memcpy( dest, src, item_size );


    //Move pointers to make place for new pointer and copy new pointer
    src = (void*) &p_n->kids[idx + 1];
    dest = (void*) &p_n->kids[idx + 2];
    memmove( dest, src, (p_n->nr_active - idx) * sizeof(bt_node_t*) );

    p_n->nr_active++;
    p_n->kids[idx + 1] = new_child;
}


/*******************************************************************************
*	Recursive function, which tries to add a new item.
*
*	@param  p_t        Pointer to the tree
*	@param  p_n        Pointer to the parent node (subtree) of interest
*	@param  item        Pointer to the new item that's gonna be added
*******************************************************************************/
char insert_item_rec(bt_t*            p_t,
                        bt_node_t*       p_n,
                        void*            item)
{
    item_counter_type   idx;
    size_t				item_size;
    compare_items_t     comp_items;

	//Remember, the btree is growing up, not down. There is no such thing as
	//a leaf node becoming a non-leaf node
    if ( is_leaf( p_n ) && is_full(p_t, p_n) )
        return 1;

    item_size		= p_t->item_size;
    comp_items		= p_t->comp_items;

	//Find item with higher key
    for (idx = 0; idx < p_n->nr_active; idx++)
    {
        void* it = get_item_pointer( p_n->items, item_size, idx );

        if (comp_items(it, item) >= 0)
            break;
    }
	
    if ( is_leaf( p_n ) )
    {
        void*               src;
        void*               dest;

		//Move higher items to the right
        src = get_item_pointer( p_n->items, item_size, idx );
        dest = get_item_pointer( p_n->items, item_size, idx + 1 );
        memmove( dest, src, (p_n->nr_active - idx) * item_size );

		//Copy in the new item
        src = item;
        dest = get_item_pointer( p_n->items, item_size, idx );
        memcpy( dest, src, item_size );

        p_n->nr_active++;

		//Success!
        return 0;
    }

    for (;;)
    {
		void* it;

		//if we were able to store in a child, then return
        if ( !insert_item_rec(p_t, p_n->kids[idx], item) )
            return 0;

		//OK, so at this point we couldn't store in a child, because it was
		//full. All right, let's split the child. Do we have enough space 
		//left for a child split?
        if ( is_full(p_t, p_n) )
            return 1;

        split_full_child( p_t, p_n, idx );

		//Now, on which side of the new item resulting from the split do we 
		//store our item?
        it = get_item_pointer( p_n->items, item_size, idx );
        if (comp_items(it, item) < 0)
            idx++;
    }
}


/*******************************************************************************
*	Adds a new item to the tree. Not the fastest method, but this way the tree
*   has a smaller memory footprint. Alternative from wikipedia:
*
*   An improved algorithm supports a single pass down the tree from the root to
*   the node where the insertion will take place, splitting any full nodes
*   encountered on the way. This prevents the need to recall the parent nodes
*   into memory, which may be expensive if the nodes are on secondary storage.
*
*	@param  tree        Pointer to the tree
*	@param  item        Pointer to the new item that's gonna be added
*******************************************************************************/
void insert_item(bt_t* p_t, void* item)
{
    while ( insert_item_rec( p_t, p_t->root, item ) )
    {
        bt_node_t* new_root = create_btree_node(p_t->order, p_t->item_size, 0);

//        new_root->level			= p_t->root->level + 1;
        new_root->nr_active		= 0;
        new_root->kids[0]		= p_t->root;

        p_t->root = new_root;
    }
}


/*******************************************************************************
*
*                                   DELETION
*
*******************************************************************************/
/*******************************************************************************
*	Merges two adjecent child nodes specified by index, BUT parent node stays 
*	untouched
*
*	@param  tree        Pointer to the tree the node is part of
*	@param  node        Pointer to the parent node of interest
*	@param  idx         Index of child that gets merged with its right neighbour
*   @param  item        Item to copy in between the items of two kids
*******************************************************************************/
bt_node_t* merge_nodes(bt_t*				p_t,
                       bt_node_t*			p_n,
                       item_counter_type    idx,
                       void*				item)
{
    size_t      item_size   = p_t->item_size;
    order_t     order       = p_t->order;
    bt_node_t*  ln;
    bt_node_t*  rn;
    void*       src;
    void*       dest;


    if ( idx >= p_n->nr_active )
        idx = p_n->nr_active - 1;

    ln = p_n->kids[idx];
    rn = p_n->kids[idx + 1];

	if (item)
		if (ln->nr_active + rn->nr_active + 1 > 2 * order - 1)
			return NULL;
	else
		if (ln->nr_active + rn->nr_active > 2 * order - 1)
			return NULL;

	if (item)
	{
//Copy item into the middle of left node
		src = item;
		dest = get_item_pointer( ln->items, item_size, ln->nr_active );
		memcpy( dest, src, item_size );

		ln->nr_active += 1;
	}

//Copy higher order items from right node to left node
    src = rn->items;
    dest = get_item_pointer( ln->items, item_size, ln->nr_active );
    memcpy( dest, src, rn->nr_active * item_size );

    if (!is_leaf(ln))
    {
//Copy higher order kids from right node to left node
        src = (void*) rn->kids;
        dest = (void*) &ln->kids[ln->nr_active];
        memcpy( dest, src, (rn->nr_active + 1) * sizeof(bt_node_t*) );
    }

    ln->nr_active += rn->nr_active;

    free_btree_node(rn);

    return ln;
}


/*******************************************************************************
*	Merges two adjecent child nodes specified by index of the parent's child list.
*
*	@param  tree        Pointer to the tree the node is part of
*	@param  node        Pointer to the parent node of interest
*	@param  idx         Index of child that gets merged with its right neighbour
*******************************************************************************/
bt_node_t* merge_siblings(bt_t*					p_t,
                          bt_node_t*			p_n,
                          item_counter_type     idx,
						  char					keep_item)
{
    size_t      item_size   = p_t->item_size;
    order_t     order       = p_t->order;
    void*       src;
    void*       dest;
	void*		item;


    if ( idx >= p_n->nr_active )
        idx = p_n->nr_active - 1;

	item = keep_item ? get_item_pointer( p_n->items, item_size, idx) : NULL ;
    merge_nodes( p_t, p_n, idx, item );


//At this point first child has everything second child had
//second child was removed
//Let's take care of parent

//Move higher items lower
    src = get_item_pointer( p_n->items, item_size, idx + 1 );
    dest = get_item_pointer( p_n->items, item_size, idx );
    memmove( dest, src, (p_n->nr_active - idx - 1) * item_size );

//Move higher kids lower
    src = (void*) &p_n->kids[idx + 2];
    dest = (void*) &p_n->kids[idx + 1];
    memmove( dest, src, (p_n->nr_active - idx - 1) * sizeof(bt_node_t*) );

    p_n->nr_active--;

//What if we worked on the root node?
    if (p_n->nr_active == 0 && p_t->root == p_n)
    {
        p_n = p_n->kids[idx];
        free_btree_node( p_t->root );
        p_t->root = p_n;

        return p_n;
    }

    return p_n->kids[idx];
}


/*******************************************************************************
*	Moves one item from one child up while moving one item down to adjecent
*   child.
*
*	@param  tree        Pointer to the tree the node is part of
*	@param  node        Pointer to the parent node of interest
*	@param  idx         Index of left child
*******************************************************************************/
void move_separating_item(bt_t*					p_t,
                          bt_node_t*			p_n,
                          item_counter_type     idx,
                          char					right)
{
    size_t      item_size   = p_t->item_size;
    bt_node_t*  ln;
    bt_node_t*  rn;
    void*       src;
    void*       dest;

    ln = p_n->kids[idx];
    rn = p_n->kids[idx + 1];

    if (right)
    {
//1. take care of right child
//Move items higher
		src = get_item_pointer( rn->items, item_size, 0 );
		dest = get_item_pointer( rn->items, item_size, 1 );
		memmove( dest, src, rn->nr_active * item_size );

//Move pointers higher
		if ( !is_leaf(rn) )
		{
			src = (void*) rn->kids;
			dest = (void*) &rn->kids[1];
			memmove( dest, src, (rn->nr_active + 1) * sizeof(bt_node_t*) );
		}

//Copy first item
		src = get_item_pointer( p_n->items, item_size, idx );
		dest = rn->items;
		memcpy( dest, src, item_size );

//Copy first pointer
		if ( !is_leaf(rn) )
			rn->kids[0] = ln->kids[ln->nr_active];

		rn->nr_active++;

//2. take care of parent
//Copy last item from left child
        src = get_item_pointer( ln->items, item_size, ln->nr_active - 1 );
        dest = get_item_pointer( p_n->items, item_size, idx );
        memcpy( dest, src, item_size );


//3. take care of left child

        ln->nr_active--;
    }
    // Move the key from the parent to the left child
    else
    {
//1. take care of left child
//Copy first item
        src = get_item_pointer( p_n->items, item_size, idx );
        dest = get_item_pointer( ln->items, item_size, ln->nr_active );
        memcpy( dest, src, item_size );

//Copy first pointer
        if ( !is_leaf(ln) )
            ln->kids[ln->nr_active + 1] = rn->kids[0];

        ln->nr_active++;


//2. take care of parent
//Copy first item from right child
        src = (void*) rn->items;
        dest = get_item_pointer( p_n->items, item_size, idx );
        memcpy( dest, src, item_size );


//3. take care of right child
//Move items lower
        src = get_item_pointer( rn->items, item_size, 1 );
        dest = get_item_pointer( rn->items, item_size, 0 );
        memmove( dest, src, (rn->nr_active - 1) * item_size );

//Move pointers lower
        if ( !is_leaf(rn) )
        {
            src = (void*) &rn->kids[1];
            dest = (void*) rn->kids;
            memmove( dest, src, rn->nr_active * sizeof(bt_node_t*) );
        }

        rn->nr_active--;
    }
}


/*******************************************************************************
*	Deletes an item, from leaf node.
*
*	@param  tree        Pointer to the tree the node is part of
*	@param  node        Pointer to the parent node of interest
*	@param  idx         Index of child
*******************************************************************************/
void delete_item_from_leaf(bt_t*				p_t,
							bt_node_t*			p_n,
							item_counter_type   idx)
{
    size_t item_size = p_t->item_size;

    void* src;
    void* dest;

    if(!is_leaf( p_n ))
        return;

    src = get_item_pointer( p_n->items, item_size, idx + 1 );
    dest = get_item_pointer( p_n->items, item_size, idx );
    memmove( dest, src, (p_n->nr_active - idx - 1) * item_size );

    p_n->nr_active--;
}


/*******************************************************************************
*	Returns node and index of max item.
*
*	@param  node        Pointer to the parent node (subtree) of interest
*******************************************************************************/
void* get_max_item(bt_t*       p_t,
				   bt_node_t*  p_n)
{
	size_t      item_size   = p_t->item_size;
	void*		item		= NULL;

    for(;;)
    {
        if ( !p_n )
            break;

		if ( is_leaf(p_n) )
		{
			item = get_item_pointer( p_n->items, item_size, p_n->nr_active - 1 );
            break;
		}

        p_n = p_n->kids[p_n->nr_active];
    }

    return item;
}


/*******************************************************************************
*	Returns node and index of min item.
*
*	@param  node        Pointer to the parent node (subtree) of interest
*******************************************************************************/
void* get_min_item(bt_t*       p_t,
				   bt_node_t*  p_n)
{
	size_t      item_size   = p_t->item_size;
	void*		item		= NULL;

    for(;;)
    {
        if ( !p_n )
            break;

		if ( is_leaf(p_n) )
		{
			item = get_item_pointer( p_n->items, item_size, 0 );
            break;
		}

        p_n = p_n->kids[0];
    }

    return item;
}


/****************************************************************
*	Function used to get the node containing the given key
*	@param btree The btree to be searched
*	@param key The the key to be searched
*	@return The node and position of the key within the node
*/
/*node_pos_t get_btree_node(bt_t*         tree,
                            void*       item)
{
    size_t          item_size   = tree->item_size;
    node_pos_t      node_pos;
    bt_node_t*      node        = tree->root;
    item_counter_type        idx;

    node_pos.node = NULL;
    node_pos.idx = 0;

    for (;;)
    {
        char result = 1;
        for (idx = 0; idx < node->nr_active; idx++)
        {
            void* it = get_item_pointer( node->items, item_size, idx );

            result = compare_items(it, item);
            if ( result >= 0 )
                break;
        }

        if ( result == 0 )
        {
            node_pos.node = node;
            node_pos.idx = idx;
            return node_pos;
        }

        if ( is_leaf(node) )
            return node_pos;

        node = node->kids[idx];
    }
}

*/
#define	REBALANCE_NEEDED		0
#define	NO_REBALANCE_NEEDED		1

/*******************************************************************************
*	Delete node form tree.
*
*	@param  tree        Pointer to the tree the node is part of
*******************************************************************************/
int rebalance_node(bt_t*			tree,
					bt_node_t*		node/*,
					item_counter_type		idx*/)
{
    item_counter_type	idx = 0;
	//TODO from here
	//So node is not a leaf
	bt_node_t*			rn;
	bt_node_t*			ln;
			
	ln = node->kids[i];
	rn = node->kids[i + 1];

	//If the two kids are small enough we'll just merge them
	if ( ln->nr_active + rn->nr_active <= 2 * order - 1)
	{
		merge_siblings(tree, node, i, 1);
		return REBALANCE_NEEDED;
	}

	if (i)
	{
		i--;

	}


	//TODO: if left and right kids half full
		//TODO: merge kids
		return REBALANCE_NEEDED;
					
	//TODO: pick either the left or the right child, wich ever one is more full and move one item from there to child
	return NO_REBALANCE_NEEDED;

}


/*******************************************************************************
*	Delete node form tree.
*
*	@param  tree        Pointer to the tree the node is part of
*******************************************************************************/
int delete_item(bt_t*       p_t,
                bt_node_t*  p_n,
                void*       item_to_delete)
{
    item_counter_type	idx = 0;
    compare_items_t     comp_items = p_t->comp_items;
	size_t				item_size   = p_t->item_size;
    order_t				order       = p_t->order;
    void*				src;
    void*				dest;
	
    // Find the index of the key greater than or equal
    // to the key that we would like to search
    char result;
    for (idx = 0; idx < p_n->nr_active; idx++)
    {
        void* item = get_item_pointer( p_n->items, item_size, idx );

        result = comp_items(item, item_to_delete);

		// OK, we've found the key
		if ( result == 0 )
		{
			bt_node_t*			rn;
			bt_node_t*			ln;
			void*				replacing_item;

			if ( is_leaf(p_n) )
			{
				delete_item_from_leaf(p_t, p_n, idx);
				return REBALANCE_NEEDED;
			}

			//So node is not a leaf
			ln = p_n->kids[idx];
			rn = p_n->kids[idx + 1];

			//If the two kids are small enough we'll just merge them
			if ( ln->nr_active + rn->nr_active <= 2 * order - 1)
			{
				merge_siblings(p_t, p_n, idx, 0);
				return REBALANCE_NEEDED;
			}

			//kids cannot be merged, so we pick one item from one child's subtree and replace the item to delete with it
			//Which item to pick? There are two possibilities:
			//1. The highest value item from the lower child. 
			//2. The lowest value item from the higher child.
			//We'll go for 1. This item has to be deleted from the subtree though.

			replacing_item = get_max_item(p_t, ln);

			//Copy over item to delete with replacing item
			src = replacing_item;
			dest = item;
			memcpy( dest, src, item_size );

			//Delete replacing item from subtree
			if ( delete_item(p_t, ln, replacing_item) == NO_REBALANCE_NEEDED )
				return NO_REBALANCE_NEEDED; 

			//Something happened in the subtree, rebalance may or may not be needed
			return rebalance_node(p_t, p_n, idx);
		}
		//only item with bigger key was found
		else if ( result > 0 )
		{
			if ( is_leaf(p_n) )
				//item was not found, nothing got done, just return
				return NO_REBALANCE_NEEDED;

			//if not leaf, the item may be in the subtree, do a recursive call
			if ( delete_item(p_t, p_n->kids[idx], item_to_delete) == NO_REBALANCE_NEEDED )
				return NO_REBALANCE_NEEDED;

			//Something happened in the subtree, rebalance may or may not be needed
			return rebalance_node(p_t, p_n, idx);
		}
	}

	//item was not found
	return NO_REBALANCE_NEEDED;

}



/*******************************************************************************
*******************************************************************************/


/**
*       Used to destory btree
*       @param btree The B-tree
*       @return none
*/
void btree_destroy(btree * btree)
{
    int i = 0;
    unsigned int current_level;

    bt_node * head, * tail, * node;
    bt_node * child, * del_node;

    node = btree->root;
    current_level = node->level;
    head = node;
    tail = node;

    while(true)
    {
        if(head == NULL)
        {
            break;
        }
        if (head->level < current_level)
        {
            current_level = head->level;
        }

        if(head->leaf == false)
        {
            for(i = 0 ; i < head->nr_active + 1; i++)
            {
                child = head->kids[i];
                tail->next = child;
                tail = child;
                child->next = NULL;
            }
        }
        del_node = head;
        head = head->next;
        free_bt_node(del_node);
    }

}

/**
*       Function used to search a node in a B-Tree
*       @param btree The B-tree to be searched
*       @param key Key of the node to be search
*       @return The key-value pair
*/
bt_key_val * btree_search(btree * btree,void * key)
{

    bt_key_val * key_val = NULL;
    node_pos kp = get_btree_node(btree,key);

    if(kp.node)
    {
        key_val = kp.node->key_vals[kp.index];
    }
    return key_val;
}

/**
*       Used to copy key value from source to destination
*       @param src The source key value
*       @param dst The dest key value
*       @return none
*/
static void copy_key_val(btree * btree, bt_key_val * src, bt_key_val * dst)
{
    unsigned int keysize;
    unsigned int datasize;

    keysize    = btree->key_size(src->key);
    dst->key        = (void *)mem_alloc(keysize);
    bcopy(src->key,dst->key,keysize);

    if(src->val)
    {
        datasize   = btree->data_size(src->val);
        dst->val       = (void *)mem_alloc(datasize);
        bcopy(src->val,dst->val,datasize);
    }

}

/**
*	Get the max key in the btree
*	@param btree The btree
*	@return The max key
*/
void * btree_get_max_key(btree * btree)
{
    node_pos node_pos;
    node_pos = get_max_key_pos(btree,btree->root);
    return node_pos.node->key_vals[node_pos.index]->key;
}

/**
*	Get the min key in the btree
*	@param btree The btree
*	@return The max key
*/
void * btree_get_min_key(btree * btree)
{
    node_pos node_pos;
    node_pos = get_min_key_pos(btree,btree->root);
    return node_pos.node->key_vals[node_pos.index]->key;
}

#ifdef DEBUG
unsigned long get_key_number(bt_node * node)
{
    unsigned long result = node->nr_active;

    if (!node->leaf)
    {
        int i;
        for(i = 0 ; i < node->nr_active + 1; i++)
            result += get_key_number(node->kids[i]);
    }

    return result;
}

void print_statistic_subtree(btree *btree)
{
    print("Number of keys: %d", get_key_number(btree->root));

    print("\r\n");
}

void pretty_print_subtree(btree *btree, bt_node * node, char* prefix)
{
    int i;
    for(i = 0 ; i < node->nr_active + 1; i++)
    {
        unsigned int prefix_len = prefix ? strlen(prefix) : 0;
        unsigned int str_len = prefix_len + 8;

        char* str = (char*) malloc((str_len + 1)*sizeof(char));
        str[str_len] = 0;
        memcpy(str, prefix, prefix_len);
        memcpy(str + prefix_len, "       |", 8);

        if (!node->leaf)
        {
            if ((i == 0) || (i == node->nr_active))
            {
                memcpy(str + prefix_len, "        ", 8);
            }

            pretty_print_subtree(btree, node->kids[i], str);
        }

        if (i < node->nr_active)
        {
            if (i == 0)
            {
                memcpy(str + prefix_len, "     --|", 8);
            }

            printf("%s", str);

            print(" %d", btree->value(node->key_vals[i]->key));

            print("\r\n");
        }

        free((void*) str);
    }
}


/**
*	Used to print the keys of the bt_node
*	@param node The node whose keys are to be printed
*	@return none
*/

static void print_single_node(btree *btree, bt_node * node)
{

    int i = 0;

    print(" { ");
    while(i < node->nr_active)
    {
        print("0x%x(%d) ", btree->value(node->key_vals[i]->key),
              node->level);
        i++;
    }
    print("} (0x%x,%d) ", node,node->leaf);
}

/**
*       Function used to print the B-tree
*       @param root Root of the B-Tree
*       @param print_key Function used to print the key value
*       @return none
*/

void print_subtree(btree *btree, bt_node * node)
{

    int i = 0;
    unsigned int current_level;

    bt_node * head, * tail;
    bt_node * child;

    current_level = node->level;
    head = node;
    tail = node;

    while(true)
    {
        if(head == NULL)
        {
            break;
        }
        if (head->level < current_level)
        {
            current_level = head->level;
            print("\n");
        }
        print_single_node(btree,head);

        if(head->leaf == false)
        {
            for(i = 0 ; i < head->nr_active + 1; i++)
            {
                child = head->kids[i];
                tail->next = child;
                tail = child;
                child->next = NULL;
            }
        }
        head = head->next;
    }
    print("\n");
}


#endif
