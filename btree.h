#ifndef _BTREE_H_
#define _BTREE_H_

///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
//
// Platform dependent headers
//
///////////////////////////////////////////////////////////

#include <stddef.h>


#define DEBUG


///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
//
// Typedefs and Structures
//
///////////////////////////////////////////////////////////

//B-tree node definition
typedef unsigned short		item_counter_type;
typedef unsigned char		order_t;


typedef struct bt_node_st	bt_node_t; //Needed for the structure to be able to selfreference itself

typedef struct bt_node_st
{
//    unsigned long long  level; //TODO WTF is this? Probably not needed
    item_counter_type	nr_active;

    void*               items;
    bt_node_t**         kids;

//    bt_node_t*          next; //TODO WTF is this? Probably not needed
} bt_node_t;

//B-tree definition
typedef char				(*compare_items_t)(void* p_item1, void* p_item2);    // Comparing items
typedef void				(*print_key_t)(void* p_item);                             // Print the key of an item

typedef struct bt_st
{
    bt_node_t*          root;           // Root node of the B-Tree
    order_t             order;          // 2*order = max active kids number; 2*order-1 = max item number
    size_t              item_size;

	//function pointers
    compare_items_t     comp_items;  // Comparing items
    print_key_t         print_key;      // Print the key of an item
} bt_t;


///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
//
// Functions
//
///////////////////////////////////////////////////////////
/*
bt_t*			create_btree(
    order_t             order,          // 2*order = max active kids number
    size_t              item_size,

//function pointers
    compare_items_t     compare_items,  // Comparing items
    print_key_t         print_key      // Print the key of an item
                );

int				btree_insert_key		(btree* btree, bt_key_val* key_val);
int				btree_delete_key		(btree* btree, bt_node* subtree, void * key);
bt_key_val*		btree_search			(btree* btree, void* key);
void			btree_destroy			(btree* btree);
void*			btree_get_max_key		(btree* btree);
void*			btree_get_min_key		(btree* btree);

void			print_statistic_subtree	(btree *btree);
#ifdef DEBUG
void			pretty_print_subtree(btree *btree, bt_node * node, char* prefix);
void			print_subtree(btree * btree,bt_node * node);
#endif

*/

#endif
